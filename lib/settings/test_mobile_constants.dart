class TestMobileConstants {
  static const ROUTE_NAME_LOGIN = "login-screen";
  static const ROUTE_NAME_MENU = "menu-screen";
  static const ROUTE_NAME_SCHEDULE_DETAIL = "cinema-schedule-detail-screen";

  //Generic constants
  static const alert_title_error = "Error";
  static const close_button = "Cerrar";
  static const enter_button = "Entrar";


  //Login constants
  static const login_user = "Usuario";
  static const login_pass = "Contraseña";
  static const login_empty_value = "Por favor introduce un valor";
  static const login_error_value = "Error al hacer login, veirifca tus datos";


  //MenuScreen constants
  static const menu_profile = "Perfil";
  static const menu_cinema_schedule = "Cartelera";


  //CinemaScheduleDetailScreen constants
  static const cinema_schedule_detail_title = "Detalle de Película";
  static const cinema_schedule_detail_name = "Nombre";
  static const cinema_schedule_detail_class = "Clasificación";
  static const cinema_schedule_detail_gen = "Género";
  static const cinema_schedule_detail_duration = "Duración";
  static const cinema_schedule_detail_sinopsis = "Sinopsis";


}