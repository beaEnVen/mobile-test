class Profile {
  String email;
  String name ;
  String phoneNumber;
  String profilePicture;
  String cardNumber;



  Profile({this.email, this.name, this.phoneNumber, this.profilePicture, this.cardNumber});

  Profile.fromMap(Map<String, dynamic> map) {
    email = map['email'];
    name = map['name'];
    phoneNumber = map['phone_number'];
    profilePicture = map['profile_picture'];
    cardNumber = map['card_number'];
  }

}

