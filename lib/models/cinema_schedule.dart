import 'dart:convert';

class Movies {
  int id;
  String rating;
  List<Media> media;
  String genre;
  String synopsis;
  String length;
  String releaseDate;
  String distributor;
  String name;
  String code;
  String originalName;

  Movies(
      {this.id, this.rating, this.genre, this.synopsis, this.length, this.releaseDate, this.distributor, this.name, this.code, this.originalName});

  Movies.fromMap(Map<String, dynamic> map) {

    var mediaJson = map['media'] as List;
    List<Media> mediaList = mediaJson.map((media) => Media.fromMap(media)).toList();
    media = mediaList;
    id = map['id'];
    code = map['code'];
    genre = map['genre'];
    synopsis = map['synopsis'];
    length = map['length'];
    releaseDate = map['release_date'];
    distributor = map['distributor'];
    name = map['name'];
    code = map['code'];
    originalName = map['original_name'];
  }
}

class Media {
  String resource;
  String type;
  String code;

  Media({this.resource, this.type, this.code});

  Media.fromMap(Map<String, dynamic> map) {
    resource = map['resource'];
    type = map['type'];
    code = map['code'];
  }
}

class CinemaSchedule {
  List<Movies> movies;
  List<Routes> routes;

  CinemaSchedule({
    this.movies,
    this.routes
  });

  CinemaSchedule.fromMap(Map<String, dynamic> map) {
    var routesJson = map['routes'] as List;
    List<Routes> routesList = routesJson.map((route) => Routes.fromMap(route)).toList();
    routes = routesList;
    var moviesJson = map['movies'] as List;
    List<Movies> moviesList = moviesJson.map((movie) => Movies.fromMap(movie)).toList();
    movies = moviesList;
  }
}

class Sizes {
  String large;
  String medium;
  String small;
  String xLarge;

  Sizes({this.large, this.medium, this.small, this.xLarge});

  Sizes.fromMap(Map<String, dynamic> map) {
    large = map['large'];
    medium = map['medium'];
    small = map['small'];
    xLarge = map['x-large'];
  }
}

class Routes {
  String code;
  Sizes sizes;

  Routes({this.code, this.sizes});

  Routes.fromMap(Map<String, dynamic> map) {
    var sizesJson = map['sizes'];
    sizes = Sizes.fromMap(sizesJson);
    code = map['code'];
  }
}

class CinemaScheduleResponse {
  int statusCode;
  String message;
  CinemaSchedule cinemaSchedule;

  CinemaScheduleResponse({
    this.statusCode,
    this.message,
    this.cinemaSchedule,
  });
}
