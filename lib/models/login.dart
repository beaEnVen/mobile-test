

class Login {
  String accessToken;
  int expiresIn;

  String refreshToken;
  String clientId;
  String userName;
  String countryCode;
  String issued;
  String expires;

  Login(
      {this.accessToken,
      this.expiresIn,
      this.refreshToken,
      this.clientId,
      this.userName});

  Login.fromMap(Map<String, dynamic> map) {
    accessToken = map['access_token'];
    expiresIn = map['expires_in'];
    refreshToken = map['refresh_token'];
    clientId = map['as:client_id'];
    userName = map['username'];
    countryCode = map['country_code'];
    issued = map['.issued'];
    expires = map['.expires'];
  }
}

class LoginResponse {
  int statusCode;
  String message ;
  Login loginResponse ;
  LoginResponse({
    this.statusCode,
    this.message,
    this.loginResponse,
  });
}
