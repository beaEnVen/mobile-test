import 'package:flutter/material.dart';
import 'package:test_mobile_app/models/profile.dart';
import 'package:test_mobile_app/screens/cinema_schedule_detail_screen.dart';
import 'package:test_mobile_app/screens/cinema_schedule_screen.dart';
import 'package:test_mobile_app/screens/login_screen.dart';
import 'package:test_mobile_app/screens/menu_screen.dart';
import 'package:test_mobile_app/screens/profile_screen.dart';

void main() {
  runApp(TestMobileApp());
}
class TestMobileApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Cinepolis App",
      theme: ThemeData(primarySwatch: Colors.green),
      initialRoute: LoginScreen.ROUTE_NAME,
      routes: {
        LoginScreen.ROUTE_NAME: (ctx)=> LoginScreen(),
        MenuScreen.ROUTE_NAME: (ctx) => MenuScreen(),
        CinemaScheduleDetailScreen.routeName: (ctx) => CinemaScheduleDetailScreen()
      },
    );
  }
}


