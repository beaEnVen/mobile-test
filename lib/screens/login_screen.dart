import 'package:flutter/material.dart';
import 'package:test_mobile_app/resources/login_service.dart';
import 'package:test_mobile_app/screens/menu_screen.dart';
import 'package:test_mobile_app/settings/test_mobile_constants.dart';

class LoginScreen extends StatefulWidget {
  static const ROUTE_NAME = TestMobileConstants.ROUTE_NAME_LOGIN;

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _formKey = GlobalKey<FormState>();
  LoginService loginService = LoginService();
  TextEditingController _userController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    return Scaffold(
      body: Form(
        key: _formKey,
        child: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                width: deviceSize.width - 40,
                child: TextFormField(
                  textInputAction: TextInputAction.next,
                  controller: _userController,
                  decoration: InputDecoration(
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.blueGrey,
                      ),
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    hintText: TestMobileConstants.login_user,
                  ),
                  validator: (value) {
                    if (value.isEmpty) {
                      return TestMobileConstants.login_empty_value;
                    }
                    return null;
                  },
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(vertical: 20),
                width: deviceSize.width - 40,
                child: TextFormField(
                  controller: _passwordController,
                  obscureText: true,
                  textInputAction: TextInputAction.done,
                  decoration: InputDecoration(
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.blueGrey,
                      ),
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    hintText: TestMobileConstants.login_pass,
                    suffixIcon: Icon(Icons.lock),
                  ),
                  validator: (value) {
                    if (value.isEmpty) {
                      return TestMobileConstants.login_empty_value;
                    }
                    return null;
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 16.0),
                child: ElevatedButton(
                  onPressed: () {
                    // Validate returns true if the form is valid, or false
                    // otherwise.
                    if (_formKey.currentState.validate()) {
                      print("Entra a servicio");
                      login(_userController.text, _passwordController.text);
                    }
                  },
                  child: Text(TestMobileConstants.enter_button),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void login(String user, String password) {
    loginService.login(user, password).then((value) {
      if (value.statusCode == 1) {
        _showAlertDialog(value.message);
      } else {
        navigateToProfile();
      }
    });
  }

  void navigateToProfile() {
    Navigator.of(context).pushNamed(MenuScreen.ROUTE_NAME);
  }

  void _showAlertDialog(String msg) {
    showDialog(
        context: context,
        builder: (buildcontext) {
          return AlertDialog(
            title: Text(TestMobileConstants.alert_title_error),
            content: Text(TestMobileConstants.login_error_value),
            actions: <Widget>[
              RaisedButton(
                color: Colors.blueGrey,
                child: Text(
                  TestMobileConstants.close_button,
                  style: TextStyle(color: Colors.white),
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        });
  }
}
