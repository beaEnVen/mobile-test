import 'package:flutter/material.dart';
import 'package:test_mobile_app/settings/test_mobile_constants.dart';

class CinemaScheduleDetailScreen extends StatelessWidget {
  static const routeName = TestMobileConstants.ROUTE_NAME_SCHEDULE_DETAIL;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blueGrey,
        title: new Center(
          child: Text(
            TestMobileConstants.cinema_schedule_detail_title,
            textAlign: TextAlign.start,
          ),
        ),
      ),
      body: Center(
        child: Text("Detalle de pelicula"),
      ),
    );
  }
}
