import 'package:flutter/material.dart';
import 'package:test_mobile_app/screens/profile_screen.dart';
import 'package:test_mobile_app/settings/test_mobile_constants.dart';

import 'cinema_schedule_screen.dart';
class MenuScreen extends StatefulWidget {
  static const ROUTE_NAME = TestMobileConstants.ROUTE_NAME_MENU;
  @override
  _MenuScreenState createState() => _MenuScreenState();
}

class _MenuScreenState extends State<MenuScreen> {
  int _selectedIndex = 0;
  final List<Widget> _children = [
    ProfileScreen(),
    CinemaScheduleScreen()
  ];

  final List<String> _title = [
    TestMobileConstants.menu_profile,
    TestMobileConstants.menu_cinema_schedule
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }
  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blueGrey,
        title: new Center(child: Text(_title[_selectedIndex],textAlign: TextAlign.start,)),
      ),
      body: _children[_selectedIndex],
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.account_circle_sharp),
            label: TestMobileConstants.menu_profile,
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.airplay_outlined),
            label:TestMobileConstants.menu_cinema_schedule,
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.blueGrey,
        onTap: _onItemTapped,
      ),
    );
  }
}
