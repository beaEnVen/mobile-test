import 'package:flutter/material.dart';
import 'package:test_mobile_app/models/cinema_schedule.dart';
import 'package:test_mobile_app/resources/cinema_schedule_service.dart';
import 'package:test_mobile_app/screens/cinema_schedule_item.dart';

class CinemaScheduleScreen extends StatefulWidget {

  @override
  _CinemaScheduleScreenState createState() => _CinemaScheduleScreenState();
}

class _CinemaScheduleScreenState extends State<CinemaScheduleScreen> {
  CinemaScheduleService cinemaScheduleService = CinemaScheduleService();
  List<Movies> movies = [];
  List<Routes> routes = [];
  @override
  void initState() {
    _init();
    super.initState();
  }
  void _init() {
    this._fetchCinemaSchedule() ;
  }
  void _fetchCinemaSchedule() {
    cinemaScheduleService.getCinemaSchedule().then((value) {
      print(value.message);
      if (value.statusCode == 0) {
        setState(() {
          movies = value.cinemaSchedule.movies;
          routes = value.cinemaSchedule.routes;
        });
      }

    });
  }
  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      body: GridView(
          padding: EdgeInsets.all(15),
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            childAspectRatio: 3 / 2,
            crossAxisSpacing: 20,
            mainAxisSpacing: 20,
          ),
          children: movies
              .map((movie) =>
             CinemaScheduleItem(movie,routes))
              .toList()),
    );
  }
}
