import 'package:flutter/material.dart';
import 'package:test_mobile_app/models/cinema_schedule.dart';
import 'package:test_mobile_app/settings/test_mobile_constants.dart';

class CinemaScheduleItem extends StatefulWidget {
   final Movies movies;
   final List<Routes> routes;

   CinemaScheduleItem(this.movies,this.routes);

  @override
  _CinemaScheduleItemState createState() => _CinemaScheduleItemState();
}

class _CinemaScheduleItemState extends State<CinemaScheduleItem> {


  @override
  Widget build(BuildContext context) {
    final route = widget.routes.firstWhere((route) => route.code == 'poster');
    final imageUrl = route.sizes.small;
    return ClipRRect(
      borderRadius: BorderRadius.circular(10),
      child: GridTile(
        child: GestureDetector(
          onTap: () => selectItem(context, ""),
          child: Image.network(imageUrl+widget.movies.media[0].resource,
            fit: BoxFit.cover,
          ),
        ),
        footer: Container(
          height: 40,
          child: GridTileBar(
            backgroundColor: Colors.black54,
            title: Text(
              widget.movies.name,
              style: TextStyle(
                fontSize: 15,
              ),
              softWrap: true,
              overflow: TextOverflow.fade,
              textAlign: TextAlign.center,
            ),
          ),
        ),
      ),
    );
  }

  selectItem(BuildContext context, Object arguments) {
    Navigator.of(context)
        .pushNamed(TestMobileConstants.ROUTE_NAME_SCHEDULE_DETAIL, arguments: arguments);
  }
}
