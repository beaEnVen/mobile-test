import 'dart:async';
import 'dart:convert';
import 'package:test_mobile_app/models/login.dart';
import 'package:test_mobile_app/resources/rest_api_service.dart';
import 'package:test_mobile_app/settings/configuration.dart';

class LoginService {
  RestApiService restClientService = RestApiService();

  Future<LoginResponse> login(String user, String password) async {
    GenericResponse response =
        await restClientService.post(user, password, LOGIN_URL);
    return _loginResponseFromJson(response.statusCode, response.message,
        (response.statusCode == 0) ? response.data : null);
  }

  LoginResponse _loginResponseFromJson(
      int statusCode, String message, String json) {
    var loginResponse = new LoginResponse();
    loginResponse.statusCode = statusCode;
    loginResponse.message = message;
    loginResponse.loginResponse = (json != null)
        ?  Login.fromMap(jsonDecode(json))
        : LoginResponse();
    return loginResponse;
  }
}
