import 'dart:convert';

import 'package:http/http.dart' as http;
import 'dart:async';
import '../settings/configuration.dart';

class RestApiService {
  final _headers = {'api_key': 'stage_4V78Fwm_android'};

  Map body = {
    'country_code': COUNTRY_CODE,
    'grant_type': GRANT_TYPE,
    'client_id': CLIENT_ID,
    'client_secret': CLIENT_SECRET
  };

  Future<GenericResponse> get(String service) async {
    try {
      final response = await http.get(
        BASE + service,
        headers: _headers,
      );

      if (response.statusCode == 200) {
        return _genericResponseFromJson(0, "", response.body);
      } else {
        return _genericResponseFromJson(1, response.body, null);
      }
    } catch (e) {
      if (e.toString().contains("No route to host") ||
          e.toString().contains("No address associated with hostname") ||
          e.toString().contains("Connection refused") ||
          e.toString().contains("Network is unreachable")) {
        return _genericResponseFromJson(
            1,
            "Consulte la conexión de datos o wifi de su dispositivo, no es posible conectarse con el servidor.",
            null);
      } else {
        return _genericResponseFromJson(
            1,
            'Error interno. Contacte con los desarrolladores. Detalles: ${e.toString()}',
            null);
      }
    }
  }

  Future<GenericResponse> post(
      String user, String password, String service) async {
    try {
      print("response: en post");
      body["username"] = user;
      body["password"] = password;

      final response =
          await http.post(BASE + service, headers: _headers, body: body);

      print("response: ${response.body}");

      if (response.statusCode == 200) {
        return _genericResponseFromJson(0, "", response.body);
      } else {
        return _genericResponseFromJson(1, response.body, null);
      }
    } catch (e) {
      if (e.toString().contains("No route to host") ||
          e.toString().contains("No address associated with hostname") ||
          e.toString().contains("Connection refused") ||
          e.toString().contains("Network is unreachable")) {
        return _genericResponseFromJson(
            1,
            "Consulte la conexión de datos o wifi de su dispositivo, no es posible conectarse con el servidor.",
            null);
      } else {
        return _genericResponseFromJson(
            1,
            'Error interno. Contacte con los desarrolladores. Detalles: ${e.toString()}',
            null);
      }
    }
  }

  GenericResponse _genericResponseFromJson(
      int statusCode, String message, dynamic data) {
    var genericResponse = new GenericResponse();
    genericResponse.statusCode = statusCode;
    genericResponse.message = message;
    genericResponse.data = data;
    return genericResponse;
  }
}

class GenericResponse {
  int statusCode;

  String message;

  dynamic data;

  GenericResponse({this.statusCode, this.message, this.data});
}
