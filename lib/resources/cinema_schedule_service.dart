import 'dart:convert';

import 'package:test_mobile_app/models/cinema_schedule.dart';
import 'package:test_mobile_app/resources/rest_api_service.dart';
import 'package:test_mobile_app/settings/configuration.dart';

class CinemaScheduleService {
  RestApiService restClientService = RestApiService();

  Future<CinemaScheduleResponse> getCinemaSchedule() async {
    GenericResponse response =
    await restClientService.get(MOVIES_URL);
    return _cinemaScheduleResponseFromJson(response.statusCode, response.message,
        (response.statusCode == 0) ? response.data : null);
  }

  CinemaScheduleResponse _cinemaScheduleResponseFromJson(
      int statusCode, String message, String json) {
    var cinemaScheduleResponse = new CinemaScheduleResponse();
    cinemaScheduleResponse.statusCode = statusCode;
    cinemaScheduleResponse.message = message;
    cinemaScheduleResponse.cinemaSchedule = (json != null)
        ? CinemaSchedule.fromMap(jsonDecode(json))
        : CinemaSchedule();
    return cinemaScheduleResponse;
  }
}
